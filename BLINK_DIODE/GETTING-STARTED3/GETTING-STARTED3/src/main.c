#include "asf.h"
#include "stdio_serial.h"
#include "conf_board.h"
#include "conf_clock.h"
#include "inttypes.h"

/** IRQ priority for PIO (The lower the value, the greater the priority) */
// [main_def_pio_irq_prior]
#define IRQ_PRIOR_PIO    0
#define IRQ_PRIOR_TC	 1
// [main_def_pio_irq_prior]

/** LED0 blink time, LED1 blink half this time, in ms */
#define BLINK_PERIOD     1000

#define STRING_EOL    "\r"
#define STRING_HEADER "-- Getting Started Example --\r\n" \
		"-- "BOARD_NAME" --\r\n" \
		"-- Compiled: "__DATE__" "__TIME__" --"STRING_EOL

/** LED0 blinking control. */
// [main_var_led0_control]
volatile bool g_b_led0_active = true;
volatile bool is_new_click = true;
volatile int buttonState = 0;
volatile bool static_time = false;
// [main_var_led0_control]

// [var_delay_after_blink_or_click]
// 1 = one TC0 interrupt = 250ms
//static
int click_time = 4;
int blink_time = 40;
//non-static
int post_click_delay = 3;
int post_blink_delay = 750; // post blink is in ms
// [var_delay_after_blink_or_click]


/** Global g_ul_ms_ticks in milliseconds since start of application */
// [main_var_ticks]
volatile uint32_t g_ul_ms_ticks = 0;
// [main_var_ticks]

/// @cond 0
/**INDENT-OFF**/
#ifdef __cplusplus
extern "C" {
#endif
/**INDENT-ON**/
/// @endcond

/**
 *  \brief Process Buttons Events
 *
 *  Change active states of LEDs when corresponding button events happened.
 */

/**
 *  \brief Handler for System Tick interrupt.
 *
 *  Process System Tick Event
 *  Increments the g_ul_ms_ticks counter.
 */
// [main_systick_handler]
void SysTick_Handler(void)
{
	g_ul_ms_ticks++;
}
// [main_systick_handler]

// [main_ms_delay]
static void mdelay(uint32_t ul_dly_ticks)
{
	uint32_t ul_cur_ticks;

	ul_cur_ticks = g_ul_ms_ticks;
	while ((g_ul_ms_ticks - ul_cur_ticks) < ul_dly_ticks);
}
// [main_ms_delay]

/**
 *  \brief Handler for Button 1 rising edge interrupt.
 *
 *  Handle process led1 status change.
 */
volatile int interrupt_counter = 0;
volatile int click_counter = 0;
volatile bool first_click = true;
volatile int tmp_TC_interrupt_counter = 0;

static void Click_service(void){
	if (static_time){
		if (first_click){
			tmp_TC_interrupt_counter = interrupt_counter;
			first_click = false;
		}
		if (interrupt_counter - tmp_TC_interrupt_counter <= click_time){
			click_counter++;
		}
		else{
			first_click = true;
			click_counter = 1;
		}
	}
	else{
		if (interrupt_counter <= 3) {
			click_counter++;
		}
		else click_counter = 1;
		interrupt_counter = 0;
	}
	printf("|%d| ", click_counter);
}

// [main_button1_handler]
static void Button1_Handler(uint32_t id, uint32_t mask)
{
	if (PIN_PUSHBUTTON_1_ID == id && PIN_PUSHBUTTON_1_MASK == mask) {
		Click_service();
	}
}
// [main_button1_handler]



/**
 *  \brief Configure the Pushbuttons
 *
 *  Configure the PIO as inputs and generate corresponding interrupt when
 *  pressed or released.
 */
static void configure_buttons(void)
{
// [main_button1_configure]
	/* Configure Pushbutton 1 */
	pmc_enable_periph_clk(PIN_PUSHBUTTON_1_ID);
	pio_set_debounce_filter(PIN_PUSHBUTTON_1_PIO, PIN_PUSHBUTTON_1_MASK, 10);
	/* Interrupt on rising edge  */
	pio_handler_set(PIN_PUSHBUTTON_1_PIO, PIN_PUSHBUTTON_1_ID,
			PIN_PUSHBUTTON_1_MASK, PIN_PUSHBUTTON_1_ATTR, Button1_Handler);
	NVIC_EnableIRQ((IRQn_Type) PIN_PUSHBUTTON_1_ID);
	pio_handler_set_priority(PIN_PUSHBUTTON_1_PIO,
			(IRQn_Type) PIN_PUSHBUTTON_1_ID, IRQ_PRIOR_PIO);
	pio_enable_interrupt(PIN_PUSHBUTTON_1_PIO, PIN_PUSHBUTTON_1_MASK);
// [main_button1_configure]
}

/**
 *  Interrupt handler for TC0 interrupt. Toggles the state of LED.
 */
// [main_tc0_handler]
void TC0_Handler(void)
{
	volatile uint32_t ul_dummy;

	/* Clear status bit to acknowledge interrupt */
	ul_dummy = tc_get_status(TC0, 0);

	/* Avoid compiler warning */
	UNUSED(ul_dummy);
	
	interrupt_counter++;
	//printf("tick\n");
}
// [main_tc0_handler]

/**
 *  Configure Timer Counter 0 to generate an interrupt every 250ms.
 */
// [main_tc_configure]
static void configure_tc(void)
{
	uint32_t ul_div;
	uint32_t ul_tcclks;
	uint32_t ul_sysclk = sysclk_get_cpu_hz();
	uint32_t counts;
	uint32_t freq = 4;

	/* Configure PMC */
	pmc_enable_periph_clk(ID_TC0);

	/** Configure TC for a 4Hz frequency and trigger on RC compare. */
	
	tc_find_mck_divisor(freq, ul_sysclk, &ul_div, &ul_tcclks, ul_sysclk);
	tc_init(TC0, 0, ul_tcclks | TC_CMR_CPCTRG);

	counts = (ul_sysclk / ul_div) / freq;

	tc_write_rc(TC0, 0, counts);

	/* Configure and enable interrupt on timer overflow. */
	NVIC_SetPriority((IRQn_Type) ID_TC0, 0);
	NVIC_EnableIRQ((IRQn_Type) ID_TC0);
	tc_enable_interrupt(TC0, 0, TC_IER_CPCS);
	tc_start(TC0, 0);
}
// [main_tc_configure]

/**
 *  Configure UART console.
 */
// [main_console_configure]
static void configure_console(void)
{
	const usart_serial_options_t uart_serial_options = {
		.baudrate = CONF_UART_BAUDRATE,
#ifdef CONF_UART_CHAR_LENGTH
		.charlength = CONF_UART_CHAR_LENGTH,
#endif
		.paritytype = CONF_UART_PARITY,
#ifdef CONF_UART_STOP_BITS
		.stopbits = CONF_UART_STOP_BITS,
#endif
	};

	/* Configure console UART. */
	sysclk_enable_peripheral_clock(CONSOLE_UART_ID);
	stdio_serial_init(CONF_UART, &uart_serial_options);
}

// [main_console_configure]

/**
 * \brief Wait for the given number of milliseconds (using the g_ul_ms_ticks
 * generated by the SAM's microcontrollers's system tick).
 *
 * \param ul_dly_ticks  Delay to wait for, in milliseconds.
 */


static void blink(int blink_count, int delay){
	int tmp_TC_interrupt = interrupt_counter;
	for(int i = 0; i < blink_count * 2; i++)
	{
		if (static_time && interrupt_counter - tmp_TC_interrupt >= blink_time) break;
		ioport_toggle_pin_level(LED0_GPIO);
		mdelay(delay);
		if (i % 2 == 0) printf("%d", i / 2);
	}
	ioport_set_pin_level(LED0_GPIO, true);
	if (static_time){
		while (interrupt_counter - tmp_TC_interrupt <= blink_time){
			
		}
	}
	else mdelay(post_blink_delay);
}

/**
 *  \brief getting-started Application entry point.
 *
 *  \return Unused (ANSI-C compatibility).
 */
// [main]

int main(void)
{
//! [main_step_sys_init]
	/* Initialize the SAM system */
	sysclk_init();
	board_init();
//! [main_step_sys_init]

//! [main_step_console_init]
	/* Initialize the console uart */
	configure_console();
//! [main_step_console_init]
	configure_tc();

	/* Output example information */
	puts(STRING_HEADER);

	/* Configure systick for 1 ms */
	puts("Configure system tick to get 1ms tick period.\r");
//! [main_step_systick_init]
	if (SysTick_Config(sysclk_get_cpu_hz() / 1000)) {
		puts("-F- Systick configuration error\r");
		while (1);
	}
//! [main_step_systick_init]


	puts("Configure buttons with debouncing.\r");
//! [main_step_btn_init]
	configure_buttons();
//! [main_step_btn_init]

	printf("Press %s once to choose static blink/click time or twice to choose non-static blink/click time.\r\n",
			PUSHBUTTON_1_NAME);
			
// [Choice of the static/non-static time]
	volatile bool choice_not_made = true;
	while (choice_not_made)
	{
		if (click_counter > 0){
			if (click_counter == 1) static_time = ! static_time;
			choice_not_made = false;
			printf("Static time = %s", static_time?"true":"false");
		}
		if (interrupt_counter > 80){
			printf("Static time = %s", static_time?"true":"false");
			choice_not_made = false;
		}
	}
// [Choice of the static/non-static time]


//! [main_step_loop]
	while (1) {
		/* Wait for LED to be active and 750ms after last button push*/
		while (click_counter == 0 || interrupt_counter <= post_click_delay){
		}
		
		blink(click_counter, 500);
	}
//! [main_step_loop]
}
// [main]
/// @cond 0
/**INDENT-OFF**/
#ifdef __cplusplus
{

}
#endif
/**INDENT-ON**/
/// @endcond
