var indexSectionsWithContent =
{
  0: "_abcdefghiklmnopqrstuvwxz",
  1: "_acdeghimnpqrstuwx",
  2: "abcdefghilmopqrstuwx",
  3: "abdimnprstu",
  4: "_abcdefghiklmnopqrstuvwxz",
  5: "bfisu",
  6: "abdgips",
  7: "abcdefghimnopqrstuwx",
  8: "_abcilmpqrstuw",
  9: "abcdefghilmnopqrstuvw",
  10: "acdemqu"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "groups",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Modules",
  10: "Pages"
};

