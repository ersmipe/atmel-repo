var searchData=
[
  ['matrix_20functions',['Matrix Functions',['../group__group_matrix.html',1,'']]],
  ['megaavr_20parts',['megaAVR parts',['../group__mega__part__macros__group.html',1,'']]],
  ['mmu_20initialization',['MMU Initialization',['../group__mmu.html',1,'']]],
  ['matrix_20_28matrix_29',['Matrix (MATRIX)',['../group__sam__drivers__matrix__group.html',1,'']]],
  ['mpu_20_2d_20memory_20protect_20unit',['MPU - Memory Protect Unit',['../group__sam__drivers__mpu__group.html',1,'']]]
];
