var searchData=
[
  ['eefc_5ffcr',['EEFC_FCR',['../struct_efc.html#a38dbb840f7e6d6d6636470906c77d2dd',1,'Efc']]],
  ['eefc_5ffmr',['EEFC_FMR',['../struct_efc.html#a0267dc569c60c29fd05b2cc8b7ee112c',1,'Efc']]],
  ['eefc_5ffrr',['EEFC_FRR',['../struct_efc.html#a82c4cb433e72e8bbd32412d06a09beb8',1,'Efc']]],
  ['eefc_5ffsr',['EEFC_FSR',['../struct_efc.html#aaf6b92c6ac0bdc78197ca0a16d37fdb0',1,'Efc']]],
  ['eefc_5fversion',['EEFC_VERSION',['../struct_efc.html#a1683a46078ca51259ce11efb449397a2',1,'Efc']]],
  ['eefc_5fwpmr',['EEFC_WPMR',['../struct_efc.html#a84e499108c143fc119dc46fab3ebae5f',1,'Efc']]],
  ['energy',['energy',['../structarm__lms__norm__instance__f32.html#a6a4119e4f39447bbee31b066deafa16f',1,'arm_lms_norm_instance_f32::energy()'],['../structarm__lms__norm__instance__q31.html#a3c0ae42869afec8555dc8e3a7ef9b386',1,'arm_lms_norm_instance_q31::energy()'],['../structarm__lms__norm__instance__q15.html#a1c81ded399919d8181026bc1c8602e7b',1,'arm_lms_norm_instance_q15::energy()']]],
  ['exccnt',['EXCCNT',['../struct_d_w_t___type.html#ac0801a2328f3431e4706fed91c828f82',1,'DWT_Type']]]
];
