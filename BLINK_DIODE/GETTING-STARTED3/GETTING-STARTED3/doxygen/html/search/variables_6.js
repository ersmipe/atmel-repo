var searchData=
[
  ['ffcr',['FFCR',['../struct_t_p_i___type.html#a3eb42d69922e340037692424a69da880',1,'TPI_Type']]],
  ['ffsr',['FFSR',['../struct_t_p_i___type.html#ae67849b2c1016fe6ef9095827d16cddd',1,'TPI_Type']]],
  ['fftlen',['fftLen',['../structarm__cfft__radix2__instance__q15.html#a874085647351dcf3f0de39d2b1d49744',1,'arm_cfft_radix2_instance_q15::fftLen()'],['../structarm__cfft__radix4__instance__q15.html#a5fc543e7d84ca8cb7cf6648970f21ca6',1,'arm_cfft_radix4_instance_q15::fftLen()'],['../structarm__cfft__radix2__instance__q31.html#a960199f1373a192366878ef279eab00f',1,'arm_cfft_radix2_instance_q31::fftLen()'],['../structarm__cfft__radix4__instance__q31.html#ab413d2a5d3f45fa187d93813bf3bf81b',1,'arm_cfft_radix4_instance_q31::fftLen()'],['../structarm__cfft__radix2__instance__f32.html#a2f915a1c29635c1623086aaaa726be8f',1,'arm_cfft_radix2_instance_f32::fftLen()'],['../structarm__cfft__radix4__instance__f32.html#a7e6a6d290ce158ce9a15a45e364b021a',1,'arm_cfft_radix4_instance_f32::fftLen()'],['../structarm__cfft__instance__q15.html#a5f9e1d3a8c127ee323b5e6929aeb90df',1,'arm_cfft_instance_q15::fftLen()'],['../structarm__cfft__instance__q31.html#a4406f23e8fd0bff8d555225612e2a2a8',1,'arm_cfft_instance_q31::fftLen()'],['../structarm__cfft__instance__f32.html#acd8f9e9540e3dd348212726e5d6aaa95',1,'arm_cfft_instance_f32::fftLen()']]],
  ['fftlenby2',['fftLenBy2',['../structarm__rfft__instance__f32.html#a075076e07ebb8521d8e3b49a31db6c57',1,'arm_rfft_instance_f32']]],
  ['fftlenreal',['fftLenReal',['../structarm__rfft__instance__q15.html#aac5cf9e825917cbb14f439e56bb86ab3',1,'arm_rfft_instance_q15::fftLenReal()'],['../structarm__rfft__instance__q31.html#af777b0cadd5abaf064323692c2e6693b',1,'arm_rfft_instance_q31::fftLenReal()'],['../structarm__rfft__instance__f32.html#a4219d4669699e4efdcb150ed7a0d9a57',1,'arm_rfft_instance_f32::fftLenReal()']]],
  ['fftlenrfft',['fftLenRFFT',['../structarm__rfft__fast__instance__f32.html#aef06ab665041ec36f5b25d464f0cab14',1,'arm_rfft_fast_instance_f32']]],
  ['fifo0',['FIFO0',['../struct_t_p_i___type.html#ae91ff529e87d8e234343ed31bcdc4f10',1,'TPI_Type']]],
  ['fifo1',['FIFO1',['../struct_t_p_i___type.html#aebaa9b8dd27f8017dd4f92ecf32bac8e',1,'TPI_Type']]],
  ['foldcnt',['FOLDCNT',['../struct_d_w_t___type.html#a35f2315f870a574e3e6958face6584ab',1,'DWT_Type']]],
  ['fpca',['FPCA',['../union_c_o_n_t_r_o_l___type.html#ac62cfff08e6f055e0101785bad7094cd',1,'CONTROL_Type']]],
  ['fscr',['FSCR',['../struct_t_p_i___type.html#a377b78fe804f327e6f8b3d0f37e7bfef',1,'TPI_Type']]],
  ['function0',['FUNCTION0',['../struct_d_w_t___type.html#a5fbd9947d110cc168941f6acadc4a729',1,'DWT_Type']]],
  ['function1',['FUNCTION1',['../struct_d_w_t___type.html#a3345a33476ee58e165447a3212e6d747',1,'DWT_Type']]],
  ['function2',['FUNCTION2',['../struct_d_w_t___type.html#acba1654190641a3617fcc558b5e3f87b',1,'DWT_Type']]],
  ['function3',['FUNCTION3',['../struct_d_w_t___type.html#a80bd242fc05ca80f9db681ce4d82e890',1,'DWT_Type']]]
];
