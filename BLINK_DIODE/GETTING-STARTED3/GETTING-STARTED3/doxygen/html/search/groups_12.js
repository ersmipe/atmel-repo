var searchData=
[
  ['universal_20synchronous_20asynchronous_20receiver',['Universal Synchronous Asynchronous Receiver',['../group__group__sam__drivers__usart.html',1,'']]],
  ['universal_20asynchronous_20receiver_20transceiver_20_28uart_29',['Universal Asynchronous Receiver Transceiver (UART)',['../group__sam__drivers__uart__group.html',1,'']]],
  ['universal_20synchronous_20asynchronous',['Universal Synchronous Asynchronous',['../group__sam__drivers__usart__group.html',1,'']]],
  ['universal_20asynchronous_20receiver_20transmitter',['Universal Asynchronous Receiver Transmitter',['../group___s_a_m_e70___u_a_r_t.html',1,'']]],
  ['universal_20synchronous_20asynchronous_20receiver_20transmitter',['Universal Synchronous Asynchronous Receiver Transmitter',['../group___s_a_m_e70___u_s_a_r_t.html',1,'']]],
  ['usb_20high_2dspeed_20interface',['USB High-Speed Interface',['../group___s_a_m_e70___u_s_b_h_s.html',1,'']]],
  ['usb_20transmitter_20interface_20macrocell',['USB Transmitter Interface Macrocell',['../group___s_a_m_e70___u_t_m_i.html',1,'']]]
];
