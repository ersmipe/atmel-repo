/* Training project to practice Timer counter setting and usage with diode blink. */

#include "asf.h"
#include "stdio_serial.h"
#include "conf_board.h"
#include "conf_clock.h"
#include "inttypes.h"

/** IRQ priority (The lower the value, the greater the priority) */
#define IRQ_PRIOR_PIO    0
#define IRQ_PRIOR_TC	 0

#define STRING_EOL    "\r"
#define STRING_HEADER "-- Blink example --\r\n" \
"-- "BOARD_NAME" --\r\n" \
"-- Compiled: "__DATE__" "__TIME__" --"STRING_EOL

//variable defining if static or non-static time is used - can be changed 2 seconds after every reset.
volatile bool static_time = false;

/// @cond 0
/**INDENT-OFF**/
#ifdef __cplusplus
extern "C" {
	#endif
	/**INDENT-ON**/
	/// @endcond

	//variable for recognizing first start
	volatile bool firststart = true;
	//variable used to reset var click
	volatile bool newclick = true;
	// var counting how many times was button pushed
	volatile int click = 0;
	// var for recognizing if button was pressed multiple times in the row
	volatile bool dblclick = false;

	static void Button1_Handler(uint32_t id, uint32_t mask)
	{
		if (PIN_PUSHBUTTON_1_ID == id && PIN_PUSHBUTTON_1_MASK == mask) {
			/* Choose static or non-static time (click 2 sec after reset - TC0 ch2 dependent) */
			if(firststart) static_time = true;
			
			else{
				/* if non-static time chosen, restart timer for measuring delay after button pressed */
				if (!static_time){
					tc_start(TC0, 0);
				}
				if (newclick){
					newclick = false;
					click = 1;
				}
				else {
					click++;
					if (static_time && click > 4){
						click = 4;
					}
				}
			}
		}
	}

	static void configure_button(void)
	{
		/* Configure Pushbutton 1 */
		pmc_enable_periph_clk(PIN_PUSHBUTTON_1_ID);
		pio_set_debounce_filter(PIN_PUSHBUTTON_1_PIO, PIN_PUSHBUTTON_1_MASK, 10);
		/* Interrupt on rising edge  */
		pio_handler_set(PIN_PUSHBUTTON_1_PIO, PIN_PUSHBUTTON_1_ID,
		PIN_PUSHBUTTON_1_MASK, PIN_PUSHBUTTON_1_ATTR, Button1_Handler);
		NVIC_EnableIRQ((IRQn_Type) PIN_PUSHBUTTON_1_ID);
		pio_handler_set_priority(PIN_PUSHBUTTON_1_PIO,
		(IRQn_Type) PIN_PUSHBUTTON_1_ID, IRQ_PRIOR_PIO);
		pio_enable_interrupt(PIN_PUSHBUTTON_1_PIO, PIN_PUSHBUTTON_1_MASK);
	}
	// tc servicing non-static time interrrupts
	void TC0_Handler(void)
	{
		volatile uint32_t ul_dummy;

		/* Clear status bit to acknowledge interrupt */
		ul_dummy = tc_get_status(TC0, 0);

		/* Avoid compiler warning */
		UNUSED(ul_dummy);
		
		newclick = true;
		tc_start(TC0, 1);
		tc_stop(TC0,0);
	}
	// tc used for blink
	void TC1_Handler(void)
	{
		volatile uint32_t ul_dummy;

		/* Clear status bit to acknowledge interrupt */
		ul_dummy = tc_get_status(TC0, 1);

		/* Avoid compiler warning */
		UNUSED(ul_dummy);
		/* blink led and decrement click counter */
		if (click > 0){
			if (ioport_get_pin_level(LED0_GPIO)){
				ioport_toggle_pin_level(LED0_GPIO);
			}
			else {
				ioport_toggle_pin_level(LED0_GPIO);
				click--;
			}
		}
		else{
			tc_stop(TC0, 1);
		}
	}
	// tc service for choosing between static/non-static time and service static time interrupts
	void TC2_Handler(void)
	{
		volatile uint32_t ul_dummy;

		/* Clear status bit to acknowledge interrupt */
		ul_dummy = tc_get_status(TC0, 2);

		/* Avoid compiler warning */
		UNUSED(ul_dummy);
		
		if (firststart){
			firststart = false;
			ioport_set_pin_level(LED0_GPIO, 1);
		}
		
		/* if non-static time chosen, this timer is useless - stop this timer */
		if (!static_time) tc_stop(TC0, 2);
		
		/* if static time chosen, set newclick every 2 sec and start blink */
		if (static_time){
			newclick = true;
			if (click > 0) tc_start(TC0, 1);
		}		
	}
	
	// Timer counters configuration
	static void configure_tc(void)
	{
		uint32_t ul_div;
		uint32_t ul_tcclks;
		uint32_t ul_sysclk = sysclk_get_cpu_hz();
		// calculation of top counter value
		uint32_t counts;
		// Master clock divisor for tc1 (cca 333ms)
		uint32_t tc0_0_divisor = 3;
		// Master clock divisor for tc2 (500ms)
		uint32_t tc0_1_divisor = 2;
		// Master clock divisor for tc3 (2s)
		uint32_t tc0_2_divisor = 0.5;
		/* master clock / tc divisor = interrupt generation time (ms)
		example: default master clock = 1000
				 tc0_0_divisor = 2
				 timer will generate interrupt every 500ms */
		
		// TIMER0 channel 0

		pmc_enable_periph_clk(ID_TC0);

		/** Configure TC for a desired frequency and trigger on RC compare. */
		tc_find_mck_divisor(tc0_0_divisor, ul_sysclk, &ul_div, &ul_tcclks, ul_sysclk);
		tc_init(TC0, 0, ul_tcclks | TC_CMR_CPCTRG);

		counts = (ul_sysclk / ul_div) / tc0_0_divisor;
		
		// write top counter value into register C for desired tc and channel
		tc_write_rc(TC0, 0, counts);

		/* Configure and enable interrupt on timer overflow. */
		NVIC_SetPriority((IRQn_Type) ID_TC0, IRQ_PRIOR_TC);
		NVIC_EnableIRQ((IRQn_Type) ID_TC0);
		tc_enable_interrupt(TC0, 0, TC_IER_CPCS);

		//-----------***********************--------------------
		// TIMER0 channel 1

		pmc_enable_periph_clk(ID_TC1);

		/** Configure TC for a desired frequency and trigger on RC compare. */
		tc_find_mck_divisor(tc0_1_divisor, ul_sysclk, &ul_div, &ul_tcclks, ul_sysclk);
		tc_init(TC0, 1, ul_tcclks | TC_CMR_CPCTRG);

		counts = (ul_sysclk / ul_div) / tc0_1_divisor;

		tc_write_rc(TC0, 1, counts);

		/* Configure and enable interrupt on timer overflow. */
		NVIC_SetPriority((IRQn_Type) ID_TC1, IRQ_PRIOR_TC);
		NVIC_EnableIRQ((IRQn_Type) ID_TC1);
		tc_enable_interrupt(TC0, 1, TC_IER_CPCS);

		//-----------***********************--------------------
		// TIMER0 channel 2

		pmc_enable_periph_clk(ID_TC2);

		/** Configure TC for a desired frequency and trigger on RC compare. */
		tc_find_mck_divisor(tc0_2_divisor, ul_sysclk, &ul_div, &ul_tcclks, ul_sysclk);
		tc_init(TC0, 2, ul_tcclks | TC_CMR_CPCTRG);

		counts = (ul_sysclk / ul_div) / tc0_2_divisor;

		tc_write_rc(TC0, 2, counts);

		/* Configure and enable interrupt on timer overflow. */
		NVIC_SetPriority((IRQn_Type) ID_TC2, IRQ_PRIOR_TC);
		NVIC_EnableIRQ((IRQn_Type) ID_TC2);
		tc_enable_interrupt(TC0, 2, TC_IER_CPCS);
		tc_start(TC0, 2);
	}

	//console configuration setup function
	static void configure_console(void)
	{
		const usart_serial_options_t uart_serial_options = {
			.baudrate = CONF_UART_BAUDRATE,
			#ifdef CONF_UART_CHAR_LENGTH
			.charlength = CONF_UART_CHAR_LENGTH,
			#endif
			.paritytype = CONF_UART_PARITY,
			#ifdef CONF_UART_STOP_BITS
			.stopbits = CONF_UART_STOP_BITS,
			#endif
		};

		/* Configure console UART. */
		sysclk_enable_peripheral_clock(CONSOLE_UART_ID);
		stdio_serial_init(CONF_UART, &uart_serial_options);
	}


	int main(void)
	{
		sysclk_init();
		board_init();
		configure_console();
		configure_tc();

		puts(STRING_HEADER);

		puts("Configure buttons with debouncing.\r");
		configure_button();

		printf("Press %s while %s is on to choose static time or wait until %s is off to choose non-static time.\r\n",
		PUSHBUTTON_1_NAME, LED_0_NAME, LED_0_NAME);
		ioport_set_pin_level(LED0_GPIO, 0);

		//! [main_step_loop]
		while (1) {
		}
		//! [main_step_loop]
	}
	// [main]
	/// @cond 0
	/**INDENT-OFF**/
	#ifdef __cplusplus
	{

	}
	#endif
	/**INDENT-ON**/
	/// @endcond
