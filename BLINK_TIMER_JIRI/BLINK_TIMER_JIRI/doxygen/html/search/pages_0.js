var searchData=
[
  ['advanced_20use_20case_20doing_20port_20access',['Advanced use case doing port access',['../ioport_quickstart_use_case_1.html',1,'ioport_quickstart']]],
  ['advanced_20use_20case_20_2d_20interrupt_20driven_20edge_20detection',['Advanced use case - Interrupt driven edge detection',['../sam_pio_quickstart_use_case_2.html',1,'']]],
  ['advanced_20use_20case_20_2d_20send_20a_20packet_20of_20serial_20data',['Advanced use case - Send a packet of serial data',['../serial_use_case_1.html',1,'serial_quickstart']]],
  ['advanced_20use_20case_20_2d_20peripheral_20bus_20clock',['Advanced use case - Peripheral Bus Clock',['../sysclk_quickstart_use_case_2.html',1,'']]]
];
