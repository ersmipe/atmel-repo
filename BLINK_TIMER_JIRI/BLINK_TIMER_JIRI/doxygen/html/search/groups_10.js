var searchData=
[
  ['sam3_2f4s_2f4l_2f4e_2f4n_2f4cm_2f4c_2fg_20timer_20counter_20_28tc_29_20driver',['SAM3/4S/4L/4E/4N/4CM/4C/G Timer Counter (TC) Driver',['../group__asfdoc__sam__drivers__tc__group.html',1,'']]],
  ['status_20and_20control_20registers',['Status and Control Registers',['../group___c_m_s_i_s___c_o_r_e.html',1,'']]],
  ['systick_20functions',['SysTick Functions',['../group___c_m_s_i_s___core___sys_tick_functions.html',1,'']]],
  ['system_20control_20block_20_28scb_29',['System Control Block (SCB)',['../group___c_m_s_i_s___s_c_b.html',1,'']]],
  ['system_20controls_20not_20in_20scb_20_28scnscb_29',['System Controls not in SCB (SCnSCB)',['../group___c_m_s_i_s___s_cn_s_c_b.html',1,'']]],
  ['system_20tick_20timer_20_28systick_29',['System Tick Timer (SysTick)',['../group___c_m_s_i_s___sys_tick.html',1,'']]],
  ['standard_20i_2fo_20_28stdio_29',['Standard I/O (stdio)',['../group__group__common__utils__stdio.html',1,'']]],
  ['standard_20serial_20i_2fo_20_28stdio_29',['Standard serial I/O (stdio)',['../group__group__common__utils__stdio__stdio__serial.html',1,'']]],
  ['statistics_20functions',['Statistics Functions',['../group__group_stats.html',1,'']]],
  ['support_20functions',['Support Functions',['../group__group_support.html',1,'']]],
  ['sam_20parts',['SAM parts',['../group__sam__part__macros__group.html',1,'']]],
  ['sdram_20controller',['SDRAM Controller',['../group___s_a_m_e70___s_d_r_a_m_c.html',1,'']]],
  ['static_20memory_20controller',['Static Memory Controller',['../group___s_a_m_e70___s_m_c.html',1,'']]],
  ['serial_20peripheral_20interface',['Serial Peripheral Interface',['../group___s_a_m_e70___s_p_i.html',1,'']]],
  ['synchronous_20serial_20controller',['Synchronous Serial Controller',['../group___s_a_m_e70___s_s_c.html',1,'']]],
  ['supply_20controller',['Supply Controller',['../group___s_a_m_e70___s_u_p_c.html',1,'']]],
  ['same70j19_20definitions',['SAME70J19 definitions',['../group___s_a_m_e70_j19__definitions.html',1,'']]],
  ['same70j19b_20definitions',['SAME70J19B definitions',['../group___s_a_m_e70_j19_b__definitions.html',1,'']]],
  ['same70j20_20definitions',['SAME70J20 definitions',['../group___s_a_m_e70_j20__definitions.html',1,'']]],
  ['same70j20b_20definitions',['SAME70J20B definitions',['../group___s_a_m_e70_j20_b__definitions.html',1,'']]],
  ['same70j21_20definitions',['SAME70J21 definitions',['../group___s_a_m_e70_j21__definitions.html',1,'']]],
  ['same70j21b_20definitions',['SAME70J21B definitions',['../group___s_a_m_e70_j21_b__definitions.html',1,'']]],
  ['same70n19_20definitions',['SAME70N19 definitions',['../group___s_a_m_e70_n19__definitions.html',1,'']]],
  ['same70n19b_20definitions',['SAME70N19B definitions',['../group___s_a_m_e70_n19_b__definitions.html',1,'']]],
  ['same70n20_20definitions',['SAME70N20 definitions',['../group___s_a_m_e70_n20__definitions.html',1,'']]],
  ['same70n20b_20definitions',['SAME70N20B definitions',['../group___s_a_m_e70_n20_b__definitions.html',1,'']]],
  ['same70n21_20definitions',['SAME70N21 definitions',['../group___s_a_m_e70_n21__definitions.html',1,'']]],
  ['same70n21b_20definitions',['SAME70N21B definitions',['../group___s_a_m_e70_n21_b__definitions.html',1,'']]],
  ['same70q19_20definitions',['SAME70Q19 definitions',['../group___s_a_m_e70_q19__definitions.html',1,'']]],
  ['same70q19b_20definitions',['SAME70Q19B definitions',['../group___s_a_m_e70_q19_b__definitions.html',1,'']]],
  ['same70q20_20definitions',['SAME70Q20 definitions',['../group___s_a_m_e70_q20__definitions.html',1,'']]],
  ['same70q20b_20definitions',['SAME70Q20B definitions',['../group___s_a_m_e70_q20_b__definitions.html',1,'']]],
  ['same70q21_20definitions',['SAME70Q21 definitions',['../group___s_a_m_e70_q21__definitions.html',1,'']]],
  ['same70q21b_20definitions',['SAME70Q21B definitions',['../group___s_a_m_e70_q21_b__definitions.html',1,'']]],
  ['serial_20interface_20_28serial_29',['Serial Interface (Serial)',['../group__serial__group.html',1,'']]],
  ['square_20root',['Square Root',['../group___s_q_r_t.html',1,'']]],
  ['system_20clock_20management',['System Clock Management',['../group__sysclk__group.html',1,'']]]
];
