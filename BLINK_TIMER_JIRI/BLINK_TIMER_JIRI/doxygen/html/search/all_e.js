var searchData=
[
  ['onebyfftlen',['onebyfftLen',['../structarm__cfft__radix2__instance__f32.html#a1d3d289d47443e597d88a40effd14b8f',1,'arm_cfft_radix2_instance_f32::onebyfftLen()'],['../structarm__cfft__radix4__instance__f32.html#ab9eed39e40b8d7c16381fbccf84467cd',1,'arm_cfft_radix4_instance_f32::onebyfftLen()']]],
  ['operation_5fin_5fprogress',['OPERATION_IN_PROGRESS',['../status__codes_8h.html#a751c892e5a46b8e7d282085a5a5bf151a9b25aed7ac4ef4fc3c7250543bb113c0',1,'status_codes.h']]],
  ['osc_2eh',['osc.h',['../same70_2osc_8h.html',1,'(Global Namespace)'],['../osc_8h.html',1,'(Global Namespace)']]],
  ['oscillator_20management',['Oscillator Management',['../group__osc__group.html',1,'']]],
  ['osc_5fmainck_5f12m_5frc',['OSC_MAINCK_12M_RC',['../group__osc__group.html#ga26ef3995c6d4946693856249733a2ec6',1,'osc.h']]],
  ['osc_5fmainck_5f12m_5frc_5fhz',['OSC_MAINCK_12M_RC_HZ',['../group__osc__group.html#ga52eefaa790ebfe3b47afd0ab8105feed',1,'osc.h']]],
  ['osc_5fmainck_5f4m_5frc',['OSC_MAINCK_4M_RC',['../group__osc__group.html#ga7ba28ece07e9f6636e48882f12b96618',1,'osc.h']]],
  ['osc_5fmainck_5f4m_5frc_5fhz',['OSC_MAINCK_4M_RC_HZ',['../group__osc__group.html#gae2654cc1904ff5a8b0bd9395c6e23530',1,'osc.h']]],
  ['osc_5fmainck_5f8m_5frc',['OSC_MAINCK_8M_RC',['../group__osc__group.html#ga9fd478d13a126580991fd71362683ff3',1,'osc.h']]],
  ['osc_5fmainck_5f8m_5frc_5fhz',['OSC_MAINCK_8M_RC_HZ',['../group__osc__group.html#ga04c7d6e5487b870446da73a64342b9a1',1,'osc.h']]],
  ['osc_5fmainck_5fbypass',['OSC_MAINCK_BYPASS',['../group__osc__group.html#gac4d0e1b4c40c392cc444070905ae4763',1,'osc.h']]],
  ['osc_5fmainck_5fbypass_5fhz',['OSC_MAINCK_BYPASS_HZ',['../group__osc__group.html#ga23db1fe84a13e64291673ae651056a9f',1,'osc.h']]],
  ['osc_5fmainck_5fxtal',['OSC_MAINCK_XTAL',['../group__osc__group.html#ga9dae37ba508c49e849348a1a6be02a2f',1,'osc.h']]],
  ['osc_5fmainck_5fxtal_5fhz',['OSC_MAINCK_XTAL_HZ',['../group__osc__group.html#ga0e333818aa6fa68b4d0e2ae0dc640a4b',1,'osc.h']]],
  ['osc_5fslck_5f32k_5fbypass',['OSC_SLCK_32K_BYPASS',['../group__osc__group.html#gad92e6b76eb7f0513f4bd6366c3b1239c',1,'osc.h']]],
  ['osc_5fslck_5f32k_5fbypass_5fhz',['OSC_SLCK_32K_BYPASS_HZ',['../group__osc__group.html#ga055022a4e140e23296ff3e58ba01593b',1,'osc.h']]],
  ['osc_5fslck_5f32k_5frc',['OSC_SLCK_32K_RC',['../group__osc__group.html#ga1edc392aebb29d297095860bdfdc0c71',1,'osc.h']]],
  ['osc_5fslck_5f32k_5frc_5fhz',['OSC_SLCK_32K_RC_HZ',['../group__osc__group.html#ga295fbbadd781c031753165124031c86e',1,'osc.h']]],
  ['osc_5fslck_5f32k_5fxtal',['OSC_SLCK_32K_XTAL',['../group__osc__group.html#ga48bbfb4bb0b81614665efc05a842d3d9',1,'osc.h']]],
  ['osc_5fslck_5f32k_5fxtal_5fhz',['OSC_SLCK_32K_XTAL_HZ',['../group__osc__group.html#ga033aadc82b43da2d0df02326c5e3db0b',1,'osc.h']]]
];
