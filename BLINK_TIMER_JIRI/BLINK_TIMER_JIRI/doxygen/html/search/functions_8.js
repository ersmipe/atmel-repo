var searchData=
[
  ['scb_5fcleandcache',['SCB_CleanDCache',['../group___c_m_s_i_s___core___cache_functions.html#ga55583e3065c6eabca204b8b89b121c4c',1,'core_cm7.h']]],
  ['scb_5fcleaninvalidatedcache',['SCB_CleanInvalidateDCache',['../group___c_m_s_i_s___core___cache_functions.html#ga1b741def9e3b2ca97dc9ea49b8ce505c',1,'core_cm7.h']]],
  ['scb_5fdisabledcache',['SCB_DisableDCache',['../group___c_m_s_i_s___core___cache_functions.html#ga6468170f90d270caab8116e7a4f0b5fe',1,'core_cm7.h']]],
  ['scb_5fdisableicache',['SCB_DisableICache',['../group___c_m_s_i_s___core___cache_functions.html#gaba757390852f95b3ac2d8638c717d8d8',1,'core_cm7.h']]],
  ['scb_5fenabledcache',['SCB_EnableDCache',['../group___c_m_s_i_s___core___cache_functions.html#ga63aa640d9006021a796a5dcf9c7180b6',1,'core_cm7.h']]],
  ['scb_5fenableicache',['SCB_EnableICache',['../group___c_m_s_i_s___core___cache_functions.html#gaf9e7c6c8e16ada1f95e5bf5a03505b68',1,'core_cm7.h']]],
  ['scb_5finvalidatedcache',['SCB_InvalidateDCache',['../group___c_m_s_i_s___core___cache_functions.html#gace2d30db08887d0bdb818b8a785a5ce6',1,'core_cm7.h']]],
  ['scb_5finvalidateicache',['SCB_InvalidateICache',['../group___c_m_s_i_s___core___cache_functions.html#ga50d373a785edd782c5de5a3b55e30ff3',1,'core_cm7.h']]],
  ['sysclk_5finit',['sysclk_init',['../group__sysclk__group.html#ga242399e48a97739c88b4d0c00f6101de',1,'sysclk.c']]],
  ['sysclk_5fset_5fprescalers',['sysclk_set_prescalers',['../group__sysclk__group.html#ga37ecf2eb697a2fedfff47af0b6f45562',1,'sysclk.c']]],
  ['sysclk_5fset_5fsource',['sysclk_set_source',['../group__sysclk__group.html#gaaf2d50871a130ef8546cea34ed6ca6f8',1,'sysclk.c']]],
  ['system_5finit_5fflash',['system_init_flash',['../system__same70_8c.html#a5ac933333b38c14429f8233c17203601',1,'system_init_flash(uint32_t ul_clk):&#160;system_same70.c'],['../system__same70_8h.html#a38ee6d8de47653fc055f10322c050831',1,'system_init_flash(uint32_t dw_clk):&#160;system_same70.c']]],
  ['systemcoreclockupdate',['SystemCoreClockUpdate',['../system__same70_8c.html#ae0c36a9591fe6e9c45ecb21a794f0f0f',1,'SystemCoreClockUpdate(void):&#160;system_same70.c'],['../system__same70_8h.html#ae0c36a9591fe6e9c45ecb21a794f0f0f',1,'SystemCoreClockUpdate(void):&#160;system_same70.c']]],
  ['systeminit',['SystemInit',['../system__same70_8c.html#a93f514700ccf00d08dbdcff7f1224eb2',1,'SystemInit(void):&#160;system_same70.c'],['../system__same70_8h.html#a93f514700ccf00d08dbdcff7f1224eb2',1,'SystemInit(void):&#160;system_same70.c']]],
  ['systick_5fconfig',['SysTick_Config',['../group___c_m_s_i_s___core___sys_tick_functions.html#gae4e8f0238527c69f522029b93c8e5b78',1,'core_cm7.h']]]
];
