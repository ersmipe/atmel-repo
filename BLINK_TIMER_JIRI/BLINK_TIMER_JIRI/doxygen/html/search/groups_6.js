var searchData=
[
  ['generic_20clock_20management',['Generic Clock Management',['../group__genclk__group.html',1,'']]],
  ['general_20purpose_20input_2foutput',['General Purpose Input/Output',['../group__gpio__group.html',1,'']]],
  ['generic_20board_20support',['Generic board support',['../group__group__common__boards.html',1,'']]],
  ['global_20interrupt_20management',['Global interrupt management',['../group__interrupt__group.html',1,'']]],
  ['gigabit_20ethernet_20mac',['Gigabit Ethernet MAC',['../group___s_a_m_e70___g_m_a_c.html',1,'']]],
  ['general_20purpose_20backup_20registers',['General Purpose Backup Registers',['../group___s_a_m_e70___g_p_b_r.html',1,'']]]
];
