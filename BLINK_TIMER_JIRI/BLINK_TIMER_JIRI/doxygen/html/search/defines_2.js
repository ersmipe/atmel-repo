var searchData=
[
  ['board_5fat24mac_5ftwihs',['BOARD_AT24MAC_TWIHS',['../same70__xplained_8h.html#a552b60bd55a9e818bc24a5e07eabce1a',1,'same70_xplained.h']]],
  ['board_5fat30tse_5fid_5ftwihs',['BOARD_AT30TSE_ID_TWIHS',['../same70__xplained_8h.html#a0a36d58b4ca0455ce580b890cbfe2e6b',1,'same70_xplained.h']]],
  ['board_5fat30tse_5ftwihs',['BOARD_AT30TSE_TWIHS',['../same70__xplained_8h.html#a7b191378cb5fd0935d5de2ac288ddd0e',1,'same70_xplained.h']]],
  ['board_5ffreq_5fslck_5fxtal',['BOARD_FREQ_SLCK_XTAL',['../same70__xplained_8h.html#a29729f712d94e421615d41fbf2748311',1,'same70_xplained.h']]],
  ['board_5fgmac_5fphy_5faddr',['BOARD_GMAC_PHY_ADDR',['../same70__xplained_8h.html#a5e015962fbe97288224969993aaaca54',1,'same70_xplained.h']]],
  ['board_5fili9488_5fspi',['BOARD_ILI9488_SPI',['../same70__xplained_8h.html#ab82dc70743c74b0a8c3ff7850c6b717c',1,'same70_xplained.h']]],
  ['board_5fmck',['BOARD_MCK',['../same70__xplained_8h.html#ae9cca1603768bb2e6c82bd0ccedf2b5c',1,'same70_xplained.h']]],
  ['board_5fname',['BOARD_NAME',['../same70__xplained_8h.html#a02581754b212d533d96cde56c8145c9b',1,'same70_xplained.h']]],
  ['board_5fno_5fled_5f1',['BOARD_NO_LED_1',['../conf__board_8h.html#a4827728b6f47793f892bcce4afcb9b54',1,'conf_board.h']]],
  ['board_5fno_5fpushbutton_5f2',['BOARD_NO_PUSHBUTTON_2',['../conf__board_8h.html#a1be806a7c3ba5833f5d5e771dd01c54e',1,'conf_board.h']]],
  ['board_5fnum_5fof_5fled',['BOARD_NUM_OF_LED',['../same70__xplained_8h.html#aa3640bf40a413c5cdf9c22e2726cf516',1,'same70_xplained.h']]],
  ['board_5fosc_5fstartup_5fus',['BOARD_OSC_STARTUP_US',['../same70__xplained_8h.html#a72c1c5d05c0eee527f680ead3d7c3f09',1,'same70_xplained.h']]],
  ['board_5fsdram_5faddr',['BOARD_SDRAM_ADDR',['../same70__xplained_8h.html#a6a082c5b71a209d7ff5a3e1cedae8913',1,'same70_xplained.h']]],
  ['board_5fsdram_5fsize',['BOARD_SDRAM_SIZE',['../same70__xplained_8h.html#a15799ce88006ec5d868195785bc623fc',1,'same70_xplained.h']]],
  ['button_5f0_5fname',['BUTTON_0_NAME',['../same70__xplained_8h.html#a20c6ec898ac72b49ab8e52d034ad1e2b',1,'same70_xplained.h']]]
];
