﻿# BLINK_DIODE 

## 7.3.2019
* repozitar vyzkousen
* v readme nepouzivej hacky a carky
* pred commitem vzdy aktualizuj readme.md
* commit oznac stejne, jako je nejaky radek v readme (dobre se to pak hleda)

# atmel-repo/BLINK_DIODE/GETTING-STARTED3/GETTING-STARTED3/src/main.c

## 17.3.2019
* Power code from Button_handler moved to new function Click_service. Added new function blink. User experience seems much better, but still not flawless.

## 18.3.2019
* Uspesne dokoncen ukol "kolik kliku, tolik bliknuti". Pridano cekani na dokonceni libovolneho poctu kliku.

## 24.3.2019
* Blikani zmeneno na pravidelne podle poctu kliknuti. Doba na klikani se stale pouze prodluzuje pri kazdem kliku a doba blikani se prizpusobuje pozadovanemu poctu bliknuti. Pridana kostra pro staticky cas bliku/kliku.

## 25.3.2019
* Kompletne pridana funkce prepinani statickeho/nestatickeho casu pro blik/klik.

## 5.2. 2019
* Added solution BLINK_TIMER_JIRI - Timer training project - rework of BLINK_DIODE project
* Added solution SD_playground - SD training project