/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * Bare minimum empty user application template
 *
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# "Insert system clock initialization code here" comment
 * -# Minimal main function that starts with a call to board_init()
 * -# "Insert application code here" comment
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */
/*
 * Support and FAQ: visit <a href="https://www.microchip.com/support/">Microchip Support</a>
 */

#include "asf.h"
#include "stdio_serial.h"
#include "conf_board.h"
#include "conf_clock.h"
#include "inttypes.h"

/* Define priority for TC and PIO */
#define IRQ_PRIO_PIO	0
#define IRQ_PRIO_TC0	1
#define IRQ_PRIO_TC1	2
#define IRQ_PRIO_TC2	3

#define STRING_EOL		"\r"
#define STRING_HEADER	"Learning project - blinking using multiple TC" \
		"-- "BOARD_NAME" --\r\n" \
		"-- Compiled: "__DATE__" "__TIME__" --"STRING_EOL

/* Var defining if static/non-static time for actions will be used. */
volatile bool static_time = false;

/* Push button count */
volatile int click_count = 0;

/* Global ticks in ms */
volatile uint32_t g_ul_ms_ticks = 0;

/* Necessary variables */


/// @cond 0
/**INDENT-OFF**/
#ifdef __cplusplus
extern "C" {
#endif
/**INDENT-ON**/
/// @endcond

void SysTick_Handler(void)
{
	g_ul_ms_ticks++;
}

/* Button Handler */
static void Button_Handler(uint32_t id, uint32_t mask){
	if (PIN_PUSHBUTTON_1_ID == id && PIN_PUSHBUTTON_1_MASK == mask) {
	click_count++;
	puts("button");
	}
}

/* Button config */
static void configure_button(void)
{
	/* Configure Pushbutton 1 */
	pmc_enable_periph_clk(PIN_PUSHBUTTON_1_ID);
	pio_set_debounce_filter(PIN_PUSHBUTTON_1_PIO, PIN_PUSHBUTTON_1_MASK, 10);
	/* Interrupt on rising edge  */
	pio_handler_set(PIN_PUSHBUTTON_1_PIO, PIN_PUSHBUTTON_1_ID,
	PIN_PUSHBUTTON_1_MASK, PIN_PUSHBUTTON_1_ATTR, Button_Handler);
	NVIC_EnableIRQ((IRQn_Type) PIN_PUSHBUTTON_1_ID);
	pio_handler_set_priority(PIN_PUSHBUTTON_1_PIO,
	(IRQn_Type) PIN_PUSHBUTTON_1_ID, IRQ_PRIO_PIO);
	pio_enable_interrupt(PIN_PUSHBUTTON_1_PIO, PIN_PUSHBUTTON_1_MASK);
}

/* TC0 interrupt handler */
void TC0_Handler(void){
	volatile uint32_t ul_dummy;

	/* Clear status bit to acknowledge interrupt */
	ul_dummy = tc_get_status(TC0, 0);

	/* Avoid compiler warning */
	UNUSED(ul_dummy);
	
	if (click_count > 0) {
	click_count--;
	LED_Toggle(LED0);
	}
	
	puts("TC0");
}

/* TC0 interrupt handler */
void TC4_Handler(void){
	volatile uint32_t ul_dummy;

	/* Clear status bit to acknowledge interrupt */
	ul_dummy = tc_get_status(TC1, 1);

	/* Avoid compiler warning */
	UNUSED(ul_dummy);
	
	LED_On(LED0);
	
	puts("TC1");
}

/* TC0 interrupt handler */
void TC2_Handler(void){
	volatile uint32_t ul_dummy;

	/* Clear status bit to acknowledge interrupt */
	ul_dummy = tc_get_status(TC2, 2);

	/* Avoid compiler warning */
	UNUSED(ul_dummy);
	
	puts("TC2");
}

/* TC0 configuration */
static void configure_tc0(void) {
	uint32_t ul_div;
	uint32_t ul_tcclks;
	uint32_t ul_sysclk = sysclk_get_cpu_hz();
	uint32_t counts;
	uint32_t TC0_divisor = 2;

	/* Configure PMC */
	pmc_enable_periph_clk(ID_TC0);

	/* Configure TC for a desired frequency and trigger on RC compare. */
	tc_find_mck_divisor(TC0_divisor, ul_sysclk, &ul_div, &ul_tcclks, ul_sysclk);
	tc_init(TC0, 0, ul_tcclks | TC_CMR_CPCTRG);
	counts = (ul_sysclk / ul_div) / TC0_divisor;
	tc_write_rc(TC0, 0, counts);

	/* Configure and enable interrupt on timer overflow. */
	NVIC_SetPriority((IRQn_Type) ID_TC0, IRQ_PRIO_TC0);
	NVIC_EnableIRQ((IRQn_Type) ID_TC0);
	tc_enable_interrupt(TC0, 0, TC_IER_CPCS);
	tc_start(TC0, 0);
}

/* TC1 configuration */
static void configure_tc1(void) {
	uint32_t ul_div;
	uint32_t ul_tcclks;
	uint32_t ul_sysclk = sysclk_get_cpu_hz();
	uint32_t counts;
	uint32_t TC1_divisor = 2;

	/* Configure PMC */
	pmc_enable_periph_clk(ID_TC1);

	/* Configure TC for a desired frequency and trigger on RC compare. */
	tc_find_mck_divisor(TC1_divisor, ul_sysclk, &ul_div, &ul_tcclks, ul_sysclk);
	tc_init(TC1, 1, ul_tcclks | TC_CMR_CPCTRG);
	counts = (ul_sysclk / ul_div) / TC1_divisor;
	tc_write_rc(TC1, 1, counts);

	/* Configure and enable interrupt on timer overflow. */
	NVIC_SetPriority((IRQn_Type) ID_TC1, IRQ_PRIO_TC1);
	NVIC_EnableIRQ((IRQn_Type) ID_TC1);
	tc_enable_interrupt(TC1, 1, TC_IER_CPCS);
	tc_start(TC1, 1);
}

/* TC2 configuration */
static void configure_tc2(void) {
	uint32_t ul_div;
	uint32_t ul_tcclks;
	uint32_t ul_sysclk = sysclk_get_cpu_hz();
	uint32_t counts;
	uint32_t TC2_divisor = 1;

	/* Configure PMC */
	pmc_enable_periph_clk(ID_TC2);

	/* Configure TC for a desired frequency and trigger on RC compare. */
	tc_find_mck_divisor(TC2_divisor, ul_sysclk, &ul_div, &ul_tcclks, ul_sysclk);
	tc_init(TC2, 2, ul_tcclks | TC_CMR_CPCTRG);

	counts = (ul_sysclk / ul_div) / TC2_divisor;

	tc_write_rc(TC2, 2, counts);

	/* Configure and enable interrupt on timer overflow. */
	NVIC_SetPriority((IRQn_Type) ID_TC2, IRQ_PRIO_TC2);
	NVIC_EnableIRQ((IRQn_Type) ID_TC2);
	tc_enable_interrupt(TC2, 2, TC_IER_CPCS);
	tc_start(TC2, 2);
}

/* Console configuration */
static void configure_console(void)
{
	const usart_serial_options_t uart_serial_options = {
		.baudrate = CONF_UART_BAUDRATE,
#ifdef CONF_UART_CHAR_LENGTH
		.charlength = CONF_UART_CHAR_LENGTH,
#endif
		.paritytype = CONF_UART_PARITY,
#ifdef CONF_UART_STOP_BITS
		.stopbits = CONF_UART_STOP_BITS,
#endif
	};
	/* Configure console UART. */
	sysclk_enable_peripheral_clock(CONSOLE_UART_ID);
	stdio_serial_init(CONF_UART, &uart_serial_options);
}

int main (void)
{
	/* Insert system clock initialization code here (sysclk_init()). */
	sysclk_init();
	board_init();
	configure_console();
	configure_tc0(); 
	//configure_tc1();
	//configure_tc2();

	puts(STRING_HEADER);
	
	configure_button();
	while (1)
	{
		puts("2");
	}
}

/// @cond 0
/**INDENT-OFF**/
#ifdef __cplusplus
{

}
#endif
/**INDENT-ON**/
/// @endcond
